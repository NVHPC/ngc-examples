#!/bin/bash

if [ ! -f apoa1.tar.gz ]; then
  echo "Downloading apoa1.tar.gz"
  wget -q http://www.ks.uiuc.edu/Research/namd/utilities/apoa1.tar.gz
else
  echo "apoa1.tar.gz already exists, not downloading"
fi

if [ ! -d apoa1 ]; then
  tar xf apoa1.tar.gz
  echo "apoa1 example directory unpacked"
else
  echo "apoa1 directory already exists"
fi

echo "Applying GPU optimizations"
pushd apoa1 > /dev/null

if [ ! -f apoa1_nve_cuda.namd ]; then
  echo "Downloading apoa1_nve_cuda.namd"
  wget -q http://www.ks.uiuc.edu/Research/namd/2.13/benchmarks/apoa1_nve_cuda.namd
else
  echo "apoa1_nve_cuda.namd already exists, not downloading"
fi

if [ ! -f apoa1_nve_cuda_soa.namd ]; then
  cp apoa1_nve_cuda.namd apoa1_nve_cuda_soa.namd
  sed -i '/^stepspercycle/ d' apoa1_nve_cuda_soa.namd
  sed -i '/^margin/ d' apoa1_nve_cuda_soa.namd
  printf "stepspercycle 200\n" >> apoa1_nve_cuda_soa.namd
  printf "pairlistspercycle 40\n" >> apoa1_nve_cuda_soa.namd
  printf "margin 8\n" >> apoa1_nve_cuda_soa.namd
  printf "SOAintegrate on\nCUDASOAintegrate on\n" >> apoa1_nve_cuda_soa.namd
else
  echo "apoa1_nve_cuda_soa.namd already exists, not creating"
fi

popd > /dev/null
