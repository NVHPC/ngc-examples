#!/usr/bin/env bash
# Usage: ./singularity.sh <gpu count> <image name>
set -e; set -o pipefail

GPU_COUNT=${1:-1}
SIMG=${2:-"namd_2.13-singlenode"}

echo "Downloading APOA1 Dataset..."
wget -O - https://gitlab.com/NVHCP/ngc-examples/raw/master/namd/2.13/get_apoa1.sh | bash
INPUT="/host_pwd/apoa1/apoa1.namd"

SINGULARITY="singularity exec --nv -B $(pwd):/host_pwd ${SIMG}"
NAMD2="namd2 ${INPUT}"

echo "Running APOA1 example in ${SIMG} on ${GPU_COUNT} GPUS..."
${SINGULARITY} ${NAMD2}
