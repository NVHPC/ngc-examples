#!/bin/bash

#SBATCH --nodes=2
#SBATCH --ntasks=16
#SBATCH --ntasks-per-socket=4
#SBATCH --time 00:10:00
set -e; set -o pipefail

# Load required modules
module load singularity

# Download NiO S32 example input
wget -O - https://gitlab.com/NVHCP/ngc-examples/raw/master/qmcpack/v3.5.0/get_S32.sh | bash
INPUT="/host_pwd/NiO-fcc-S32-dmc.xml"

# Change to NiO S32 example directory
cd S32_example

# Generate MPI hostfile
HOSTFILE=".hostfile.${SLURM_JOB_ID}"
for host in $(scontrol show hostnames); do
  echo "${host}" >> ${HOSTFILE}
done

# singularity alias which will launch mpirun and qmcpack
SIMG="${SLURM_SUBMIT_DIR}/qmcpack_v3.5.0.simg"
SINGULARITY="$(which singularity) run --nv -B $(pwd):/host_pwd ${SIMG}"

# mpirun alias, setup to launch through singularity
export OMPI_MCA_plm=rsh
export OMPI_MCA_plm_rsh_args="-o PubkeyAcceptedKeyTypes=+ssh-dss -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=ERROR"
export OMPI_MCA_orte_launch_agent="${SINGULARITY} orted"
MPIRUN="--check_gpu=false mpirun --hostfile /host_pwd/${HOSTFILE} -np ${SLURM_NTASKS} --map-by ppr:${SLURM_NTASKS_PER_SOCKET}:socket"

# Launch parallel qmcpack
${SINGULARITY} ${MPIRUN} qmcpack ${INPUT}

# Cleanup hostfile
rm ${HOSTFILE}
