#!/bin/bash
set -e
# Usage: $ ./water.sh {GPU_COUNT} {IMG_NAME}

# Script arguments
GPU_COUNT=${1:-1}
IMG=${2:-"gromacs:gromacs2018.2-ubuntu16.04-cuda9.0-mpi3.0.0"}

# Set number of OpenMP threads
export OMP_NUM_THREADS=${OMP_NUM_THREADS:-1}

# Create a directory on the host to work within
mkdir -p ./work
cd ./work

# Download benchmark data
DATA_SET=water_GMX50_bare
wget -c ftp://ftp.gromacs.org/pub/benchmarks/${DATA_SET}.tar.gz
tar xf ${DATA_SET}.tar.gz

# Change to the benchmark directory
cd ./water-cut1.0_GMX50_bare/1536

# Docker will mount the host PWD to /host_pwd in the container
DOCKER="nvidia-docker run -v ${PWD}:/host_pwd -w /host_pwd ${IMG}"

# Prepare benchmark data
${DOCKER} gmx grompp -f pme.mdp

# Run benchmark
${DOCKER} gmx mdrun \
    -ntmpi ${GPU_COUNT} \
    -nb gpu \
    -pin on \
    -v \
    -noconfout \
    -nsteps 5000 \
    -s topol.tpr \
    -ntomp $OMP_NUM_THREADS
