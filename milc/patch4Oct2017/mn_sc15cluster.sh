#!/bin/bash

CONTAINER=$1
NODES=$2
TASKS_PER_NODE=$3

NGPUS="$(($NODES * $TASKS_PER_NODE))"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TEST_DIR_NAME=sc15cluster
TEST_DIR=$SCRIPT_DIR/$TEST_DIR_NAME

mkdir -p $TEST_DIR
cd $TEST_DIR

# retrieve benchmark data
BMARK_ARCHIVE=benchmarks.tar
if [ ! -f $BMARK_ARCHIVE ]; then
  wget http://denali.physics.indiana.edu/~sg/SC15_student_cluster_competition/$BMARK_ARCHIVE
fi
tar xf $BMARK_ARCHIVE

cd ..

GEOM=$(./milc_geom.sh $NGPUS)
echo "Using -geom $GEOM..."

INPUT_FILE=small.bench.in
OUTPUT_FILE=small_bench_$(date +%y-%m-%d_%H-%M).out

echo "Loading required modules..."
module load Singularity

echo "Launching SC15 Cluster benchmark job. Saving output to $TEST_DIR/$OUTPUT_FILE..."
srun --job-name=$CONTAINER --nodes=$NODES \
  --ntasks-per-node=$TASKS_PER_NODE --mpi=pmi2 singularity run \
  --nv -B $TEST_DIR:/bench $CONTAINER /bin/bash -c \
  "cd /bench/small && su3_rhmd_hisq -geom $GEOM $INPUT_FILE ../$OUTPUT_FILE"
RESULT=$?

if [ $RESULT ]; then
  echo "SC15 Cluster benchmark complete, results can be found at $TEST_DIR/$OUTPUT_FILE"
else
  echo "SC15 Cluster benchmark failed! results can be found at $TEST_DIR/$OUTPUT_FILE"
fi

