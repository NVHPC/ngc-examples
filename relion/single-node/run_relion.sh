#!/bin/bash
set -euf -o pipefail

readonly iter=${RELION_ITER:-25}
readonly pool=${RELION_POOL:-100}
readonly output_dir=${RELION_OUTPUT_DIR:-${PWD}/run.$(date +%Y.%m.%d.%H.%M)}
readonly scratch_dir=${RELION_SCRATCH_DIR:-}
readonly no_preread=${RELION_NO_PREREAD:-}
readonly gpu_count=${RELION_GPU_COUNT:-$(nvidia-smi --list-gpus | wc -l)}
readonly cpu_count=${RELION_CPU_COUNT:-$(nproc)}
readonly procs_per_gpu=${RELION_GPU_WORKERS:-2}
readonly host_mps=${RELION_HOST_MPS:-}

if (( gpu_count < 2 )); then
    echo "WARNING: It is recommended to use at least two GPU's for 3D auto-refinement"
fi

gpu_list=$(seq -s, 0 "$(( gpu_count -1 ))")
export CUDA_VISIBLE_DEVICES=${gpu_list}

# Set Relion 3D classification experiment flags
relion_opts="--gpu \
             --i Particles/shiny_2sets.star \
             --ref emd_2660.map:mrc \
             --firstiter_cc \
             --ini_high 60 \
             --ctf \
             --ctf_corrected_ref \
             --tau2_fudge 4 \
             --K 6 \
             --flatten_solvent \
             --healpix_order 2 \
             --sym C1 \
             --iter ${iter} \
             --particle_diameter 360 \
             --zero_mask \
             --oversampling 1 \
             --offset_range 5 \
             --offset_step 2 \
             --norm \
             --scale \
             --random_seed 0 \
             --pool ${pool} \
             --dont_combine_weights_via_disc \
             --o ${output_dir}"

# Attempt to start MPS server within container if needed
if (( procs_per_gpu > 1 )) && [[ -z "${host_mps}" ]]; then
    export CUDA_MPS_PIPE_DIRECTORY="${PWD}/.mps"
    export CUDA_MPS_LOG_DIRECTORY="${PWD}/.mps"
    if ! nvidia-cuda-mps-control -d; then
        echo "ERROR: Failed to start MPS daemon. Please resolve issue or set RELION_GPU_WORKERS to 1"
        exit 1
    fi
    echo "INFO: MPS server daemon started"
    trap "echo quit | nvidia-cuda-mps-control" EXIT
fi

# Attempt to use as many CPU cores as possible
readonly proc_count=$(((gpu_count*procs_per_gpu)+1))
readonly tpg_max=6
readonly tpg=$(( cpu_count/proc_count  ))
readonly threads_per_process=$(( tpg <= tpg_max ? tpg : tpg_max))
relion_opts+=" --j ${threads_per_process}"

# If enough memory is available read particle data into RAM at job start
# Else if enough memory isn't available copy data to scratch space, if defined
readonly free_mem_gb=$(awk '/MemFree/ { printf "%d \n", $2/1024/1024 }' /proc/meminfo)
readonly min_mem_gb=$(( 64*proc_count ))
if (( free_mem_gb > min_mem_gb )) && [[ -z "${no_preread}" ]]; then
    echo "INFO: ${free_mem_gb} GB of system memory free, pre-reading images"
    relion_opts+=" --preread_images"
elif [[ -n "${scratch_dir}" ]]; then
    echo "INFO: scratch path will be used: ${scratch_dir}"
    relion_opts+=" --scratch_dir ${scratch_dir}"
fi

echo "INFO: Running RELION with:"
echo "  ${gpu_count} GPUs"
echo "  ${proc_count} MPI processes total"
echo "  ${procs_per_gpu} MPI processes per GPU"
echo "  ${threads_per_process} threads per worker process"

mkdir -p "${output_dir}"

# WAR for NGC 3.0.8 version
# Create libcufft soname symlink
readonly cufft_link="${PWD}/.relion_308_war"
mkdir -p "${cufft_link}"
[[ -f ${cufft_link}/libcufft.so.10 ]] || ln -fs /usr/local/cuda/lib64/libcufft.so "${cufft_link}"/libcufft.so.10
export LD_LIBRARY_PATH=${cufft_link}:${LD_LIBRARY_PATH:-}

set -x
mpirun --allow-run-as-root \
       -n ${proc_count} \
       --oversubscribe \
       relion_refine_mpi \
       ${relion_opts} \
       2>&1 | tee ${output_dir}/log.txt
